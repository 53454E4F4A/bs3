/* Description: Memory Manager BSP3*/
/* Prof. Dr. Wolfgang Fohl, HAW Hamburg */
/* Winter 2010/2011
 * 
 * This is the memory manager process that
 * works together with the vmaccess process to
 * mimic virtual memory management.
 *
 * The memory manager process will be invoked
 * via a SIGUSR1 signal. It maintains the page table
 * and provides the data pages in shared memory
 *
 * This process is initiating the shared memory, so
 * it has to be started prior to the vmaccess process
 *
 * TODO:
 * currently nothing
 * */
//#define PFDMP_STORE
//#define MEMDMP_FETCH
//#define STEP_PAGEFAULTS
#include "mmanage.h"

struct logevent log_e;
struct vmem_struct *vmem = NULL;
FILE *pagefile = NULL;
FILE *logfile = NULL;
int signal_number = 0;          /* Received signal */

int
main(void)
{
  
    struct sigaction sigact;
   
    /* Init pagefile */
    init_pagefile(MMANAGE_PFNAME);
    if(!pagefile) {
        perror("Error creating pagefile");
        exit(EXIT_FAILURE);
    }

    /* Open logfile */
    logfile = fopen(MMANAGE_LOGFNAME, "w");
    if(!logfile) {
        perror("Error creating logfile");
        exit(EXIT_FAILURE);
    }

    /* Create shared memory and init vmem structure */
    vmem_init();
    if(!vmem) {
        perror("Error initialising vmem");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "vmem successfully created\n");
    }
#endif /* DEBUG_MESSAGES */
    //printf("%d\n",sizeof(vmem->adm.bitmap));
    /* Setup signal handler */
    /* Handler for USR1 */
    sigact.sa_handler = sighandler;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;
    if(sigaction(SIGUSR1, &sigact, NULL) == -1) {
        perror("Error installing signal handler for USR1");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "USR1 handler successfully installed\n");
    }
#endif /* DEBUG_MESSAGES */

    if(sigaction(SIGUSR2, &sigact, NULL) == -1) {
        perror("Error installing signal handler for USR2");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "USR2 handler successfully installed\n");
    }
#endif /* DEBUG_MESSAGES */

    if(sigaction(SIGINT, &sigact, NULL) == -1) {
        perror("Error installing signal handler for INT");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "INT handler successfully installed\n");
    }
#endif /* DEBUG_MESSAGES */

    /* Signal processing loop */
    while(1) {
        signal_number = 0;
        pause();
        if(signal_number == SIGUSR1) {  /* Page fault */
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "Processed SIGUSR1\n");
#endif /* DEBUG_MESSAGES */
            signal_number = 0;
        }
        else if(signal_number == SIGUSR2) {     /* PT dump */
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "Processed SIGUSR2\n");
#endif /* DEBUG_MESSAGES */
            signal_number = 0;
        }
        else if(signal_number == SIGINT) {
	 
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "Processed SIGINT\n");
#endif /* DEBUG_MESSAGES */
        }
    }


    return 0;
}
void sighandler(int signo){
  
 
  switch(signo)
  {
    case SIGUSR2:
      dump_mem();
      dump_pf();
    case SIGUSR1:     
      vmem->adm.pf_count++;    
      log_e.req_pageno = vmem->adm.req_pageno;
      log_e.pf_count = vmem->adm.pf_count;
      log_e.g_count = vmem->adm.g_count;
      fetch_page(vmem->adm.req_pageno);
      logger(log_e);
      
#ifdef STEP_PAGEFAULTS
      getchar();
#endif
   
      sem_post(&vmem->adm.sema);
      
      break;
      case SIGHUP:
      case SIGINT:
      cleanup();
      printf("quitting\n");
      exit(EXIT_SUCCESS);
      break;
     
	
    
  }
}
void vmem_init(){
  int i;
  int shm_id = shmget(ftok(SHMKEY, SHMKEYID), SHMSIZE, SHM_PERMISSIONS | IPC_CREAT | IPC_EXCL);
  if(shm_id == VOID_IDX) {
    perror("vmem_init");
    exit(EXIT_FAILURE);
  } 
#ifdef DEBUG_MESSAGES
   else {
    printf("vmem_init: shm_id: %d\n", shm_id);     
  }
#endif
  vmem = shmat(shm_id, 0, 0);
  if(vmem == (struct vmem_struct *) VOID_IDX) {
    perror("vmem_init");
    exit(EXIT_FAILURE);
  }
  #ifdef DEBUG_MESSAGES
   else {
    printf("vmem_init: attached to shared memory at 0x%X\n",(unsigned int)vmem);    
  }
#endif

  vmem->adm.shm_id = shm_id;
  vmem->adm.mmanage_pid = getpid();
  vmem->adm.next_alloc_idx = 0;
  sem_init(&vmem->adm.sema, 1, 0);
  vmem->adm.pf_count = 0;
  vmem->adm.g_count = 0;
  
   for(i = 0; i < (sizeof(vmem->adm.bitmap) / sizeof(Bmword)); i++) {
  
     vmem->adm.bitmap[i] = 0;
    
  }
  
}




void fetch_page(int pt_idx){
 
  int *dest = NULL;  
  int f_idx = VOID_IDX;
  int b_written = 0;
      
      
  
  f_idx = search_bitmap();

  
  if(f_idx == VOID_IDX) {
    #ifdef VMEM_ALGO_FIFO

       f_idx = find_remove_fifo();  
    #endif
    #ifdef VMEM_ALGO_CLOCK
       f_idx = find_remove_clock();  
#endif
       #ifdef VMEM_ALGO_LRU
       f_idx = find_remove_lru();  
#endif

 
  }
  if(f_idx != VOID_IDX) {
      dest = &vmem->data[FRIDX(f_idx)];
      #ifdef DEBUG_MESSAGES  
      printf("Allocated %d bytes for page %d at %d (0x%x)\n", VMEM_PAGESIZE*sizeof(int), pt_idx, f_idx, (int) dest);      
      #endif
  }
  
  
  
  if(dest != 0) {
 
  log_e.replaced_page = vmem->pt.framepage[f_idx];
  log_e.alloc_frame = f_idx;
  
  vmem->pt.entries[pt_idx].flags = PTF_PRESENT;
  vmem->pt.entries[pt_idx].frame = f_idx;
  vmem->pt.framepage[f_idx] = pt_idx;
  
  
  #ifdef DEBUG_MESSAGES
  fseek(pagefile,(FRIDX(pt_idx))*sizeof(int), SEEK_SET);
  printf("reading file at %lu\n", ftell(pagefile));  
  #endif
  #ifdef MEMDMP_FETCH
  dump_mem();
  printf("----------------------------------------------------\n");	
  #endif
  fseek(pagefile,(FRIDX(pt_idx))*sizeof(int), SEEK_SET);
  b_written = fread(dest, sizeof(int), VMEM_PAGESIZE, pagefile); 

  #ifdef DEBUG_MESSAGES
  printf("Frame: %d, Replaced Page: %d, New page: %d, written: %d\n", f_idx, log_e.replaced_page, pt_idx, b_written);
  #endif
  #ifdef MEMDMP_FETCH
  dump_mem();
  printf("----------------------------------------------------\n");
  getchar();
  #endif     
  } else {
  cleanup();
  printf("Cannot allocate a new frame\n");
  exit(EXIT_FAILURE);
    
  }
}


void store_page(int pt_idx){
  
  int b_written = 0;
  
  if((vmem->pt.entries[pt_idx].flags & PTF_DIRTY) == PTF_DIRTY) { 
    vmem->pt.entries[pt_idx].flags ^= PTF_DIRTY;   

  
    #ifdef DEBUG_MESSAGES
    printf("swapping page %d from idx %d (0x%x) to file at %d\n", pt_idx, (FRIDX(vmem->pt.entries[pt_idx].frame)),(int)  &vmem->data[(FRIDX(vmem->pt.entries[pt_idx].frame))], (int) ftell(pagefile));
    #endif
    
    #ifdef PFDMP_STORE
    dump_pf();
    printf("----------------------------------------------------\n");
    #endif
   
    fseek(pagefile,(FRIDX(pt_idx))*sizeof(int), SEEK_SET);
    b_written = fwrite(&vmem->data[(FRIDX(vmem->pt.entries[pt_idx].frame))], sizeof(int), VMEM_PAGESIZE,pagefile);
   
    #ifdef PFDMP_STORE
    dump_pf();
    printf("----------------------------------------------------\n");
    getchar();
    #endif
    
    #ifdef DEBUG_MESSAGES
    printf("written: %d\n", b_written);
    #endif
    
  } 
}

int find_remove_fifo(void){
  int f_idx = vmem->adm.next_alloc_idx;
  store_page(vmem->pt.framepage[f_idx]);
  vmem->pt.entries[vmem->pt.framepage[f_idx]].flags = 0;
  vmem->adm.next_alloc_idx++;
  if(vmem->adm.next_alloc_idx == VMEM_NFRAMES) vmem->adm.next_alloc_idx = 0;
  return f_idx;
}

int find_remove_lru(void){
  int i = 0;
  int count = vmem->pt.entries[vmem->pt.framepage[i]].count;
  int f_idx = i;
  int reset = 0;
  if(vmem->adm.g_count >= LRU_RESET) reset = 1; 
   if(reset) vmem->pt.entries[vmem->pt.framepage[i]].count = 0;
  for(i = 1; i < VMEM_NFRAMES; i++) {
    
    if(vmem->pt.entries[vmem->pt.framepage[i]].count < count) {
      f_idx = i;
      count = vmem->pt.entries[vmem->pt.framepage[i]].count;
    }
    
    if(reset) vmem->pt.entries[vmem->pt.framepage[i]].count = 0;
    
  }
  if(reset) vmem->adm.g_count = 0;
  store_page(vmem->pt.framepage[f_idx]);
  vmem->pt.entries[vmem->pt.framepage[f_idx]].flags = 0;
  return f_idx;
}

int find_remove_clock(void){
  int f_idx = vmem->adm.next_alloc_idx;
  vmem->adm.next_alloc_idx++;
  if(vmem->adm.next_alloc_idx == VMEM_NFRAMES) vmem->adm.next_alloc_idx = 0;
  if(((vmem->pt.entries[vmem->pt.framepage[f_idx]].flags) & PTF_USED) == PTF_USED) {
  
    vmem->pt.entries[vmem->pt.framepage[f_idx]].flags ^= PTF_USED;
    
    return find_remove_clock();
    
  }
  
  store_page(vmem->pt.framepage[f_idx]);
  vmem->pt.entries[vmem->pt.framepage[f_idx]].flags = 0;
   return f_idx;
}

int search_bitmap(void){
  int i = 0;
  int free_bit = VOID_IDX; 
  
  for(i = 0; i < (sizeof(vmem->adm.bitmap) / sizeof(Bmword)); i++) {
  free_bit = find_free_bit(vmem->adm.bitmap[i], 1);
  
  if(free_bit >= 0) {
    vmem->adm.bitmap[i] |= 1 << free_bit;
     return free_bit;// + i * (sizeof(Bmword) * VMEM_PAGESIZE);
    }
  }
  return VOID_IDX;
}

int find_free_bit(Bmword bmword, Bmword mask){
  int i = 0;
  int tempmask = 0;
  for(i = 0; i < sizeof(Bmword)*8; i++) {
  tempmask = mask << i;
  if(i == VMEM_NFRAMES) return VOID_IDX;
  if((bmword & tempmask) == 0) return i;    
  }
  
  return VOID_IDX;
  }

void init_pagefile(const char *pfname){
  //printf("in pagefile\n");
  int bytes_written = 0;
  int product = 0;
  srand(SEED);
  remove(pfname);
  pagefile = fopen(pfname, "w+");
  if(pagefile != 0) {
  while(bytes_written < VMEM_VIRTMEMSIZE) {
    product = rand() % RANDMOD;
   // printf("%d\n", product);
    bytes_written += fwrite(&product, sizeof(int), 1, pagefile);    
  }
  } else {
    perror("error opening pagefile");
    cleanup();
    exit(EXIT_FAILURE);
  }
  
  
}

void cleanup(void){
    int ret;
    int shm_id;    
    if(!sem_close(&vmem->adm.sema)) {
    
      perror("sem_close\n");
    }
  #ifdef DEBUG_MESSAGES
else {
  printf("\nsemaphore closed\n");   
}
#endif
    ret = fclose(logfile);
    if(ret) {
    
      perror("fclose log");
      
    }
  #ifdef DEBUG_MESSAGES
else {
  printf("logfile closed\n");   
}
#endif
    ret = fclose(pagefile);
    if(ret) {
    
      perror("fclose pagefile");
      
    }
 #ifdef DEBUG_MESSAGES
else {
  printf("pagefile closed\n");   
}
#endif
    shm_id = vmem->adm.shm_id;
    ret = shmdt(vmem);
    if (ret==VOID_IDX)
    {
      perror("shmdt: ");
       
    } 
#ifdef DEBUG_MESSAGES
else {
  printf("shared memory detached\n");   
}
#endif
    if (shmctl(shm_id, IPC_RMID, NULL) < 0)
    {
     perror("shmctl: ");
    }
    #ifdef DEBUG_MESSAGES
else {
  printf("shared memory id removed\n");   
}
#endif
}
/* Please DO keep this function unmodified! */
void
logger(struct logevent le)
{
    fprintf(logfile, "Page fault %10d, Global count %10d:\n"
            "Removed: %10d, Allocated: %10d, Frame: %10d\n",
            le.pf_count, le.g_count,
            le.replaced_page, le.req_pageno, le.alloc_frame);
    fflush(logfile);
}
void dump_pt(void) 
{
  int i = 0;
  for(i = 0; i < VMEM_NPAGES; i++) {
  
    printf("Page %d: frame: %d flags: %d count: %d\n", i, vmem->pt.entries[i].frame, vmem->pt.entries[i].flags, vmem->pt.entries[i].count);
    
 }

  
}
void dump_mem(void)
{
  int i = 0;
  for(i = 0; i < VMEM_PHYSMEMSIZE; i++) {
  printf("%10d", vmem->data[i]);
  printf("%c", ((i+1) % NDISPLAYCOLS) ? ' ' : '\n');
  }
}
void dump_pf(void)
{
   fseek(pagefile,0, SEEK_SET);
  int i = 0;
  int buf = 0;
  while(fread(&buf, sizeof(int), 1, pagefile) == 1) {
  printf("%10d", buf);
  printf("%c", ((i+1) % NDISPLAYCOLS) ? ' ' : '\n');  
  i++;
    
  }
}

void update_pt(int frame){
}
void allocate_page(void) {
    
}

int find_remove_frame(void){
  return 0;
}
