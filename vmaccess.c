#include "vmaccess.h"

struct vmem_struct *vmem = NULL;

void vm_init(void) {
  int shm_id = shmget(ftok(SHMKEY, SHMKEYID), SHMSIZE, SHM_PERMISSIONS);
  if(shm_id == VOID_IDX) {  
    perror("vm_init: shmget");
    exit(EXIT_FAILURE);    
  }

  vmem = shmat(shm_id, NULL, 0);
  if(vmem == (struct vmem_struct *)-1) {
    perror("vm_init: shmat");
    exit(EXIT_FAILURE);
      
  }
  
}
/* Read from "virtual" address */
int vmem_read(int address){
   if(!vmem) vm_init();
   int ph_addr = vmem_transform(address);
   if(ph_addr != VOID_IDX) {
     return vmem->data[ph_addr];
   } else {   
     sem_wait(&vmem->adm.sema);
     return vmem_read(address);
  }
}

/* Write data to "virtual" address */
void vmem_write(int address, int data){
  
  if(!vmem) vm_init();
 int ph_addr = vmem_transform(address);
 if(ph_addr == VOID_IDX) {
   sem_wait(&vmem->adm.sema);
   vmem_write(address, data);
 } else {
 vmem->pt.entries[PTIDX(address)].flags |= PTF_DIRTY;
 vmem->data[ph_addr] = data;
 }
}


int vmem_transform(int address) {

  int frame_idx;
  int offset = OFFSET(address);
  int pt_idx = PTIDX(address);
  if((vmem->pt.entries[pt_idx].flags & PTF_PRESENT) != PTF_PRESENT) {
  
    vmem->adm.req_pageno = pt_idx;
    kill(vmem->adm.mmanage_pid, SIGUSR1);
     return VOID_IDX;
    //PAGE FAULT
    
  } else {
  frame_idx = FRIDX(vmem->pt.entries[pt_idx].frame);
  vmem->pt.entries[pt_idx].count++;
  vmem->adm.g_count++;
  vmem->pt.entries[pt_idx].flags |= PTF_USED; 
  return (frame_idx | offset);
    
  }
  return VOID_IDX;
  
  
}