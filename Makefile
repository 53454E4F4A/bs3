CC      = gcc
CFLAGS  = -Wall -g -DVMEM_ALGO_FIFO -DDEBUG_MESSAGES
LDFLAGS = -lpthread
BIN = mmanage vmappl
OBJM = mmanage.o
OBJA = vmappl.o vmaccess.o

all: $(BIN)

mmanage: $(OBJM)
	$(CC) $(CFLAGS) -o mmanage $(OBJM) $(LDFLAGS)

vmappl:  $(OBJA)
	$(CC) $(CFLAGS) -o vmappl $(OBJA) $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f $(BIN) *.o