/* Header file for vmappl.c
 * File: vmappl.h
 * Prof. Dr. Wolfgang Fohl, HAW Hamburg
 * 2010
 */

#ifndef VMACCESS_H
#define VMACCESS_H
#include "vmem.h"
#include <sys/ipc.h>
#include <sys/shm.h>
#define PTIDX(a) (a >> 3)
#define FRIDX(a) (a << 3)
#define OFFSET(a) (a & (VMEM_PAGESIZE-1))
#define SHMKEYID 1
#define SHM_PERMISSIONS 0666
#define VOID_IDX -1
/* Connect to shared memory (key from vmem.h) */
void vm_init(void);

/* Read from "virtual" address */
int vmem_read(int address);

/* Write data to "virtual" address */
void vmem_write(int address, int data);
int vmem_transform(int address);
#endif
